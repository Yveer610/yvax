<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|att att 
*/

Route::get('/', function () {
    return view('homepage.homepage');
});

Auth::routes();

Route::get('/contact', function(){
    return view('contact.contact');
})->name('contact');

Route::get('/product', function(){
    return view('product.product');
})->name('product');

Route::post('StoreSound', 'UploadController@store')->name('StoreSound');

Route::get('/home', 'HomeController@index')->name('home');//Route pour la page perso de l'utilisateur
    