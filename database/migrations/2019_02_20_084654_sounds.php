<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sounds extends Migration
{
    /**
     * Run the migrations.
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('sounds', function(Blueprint $table){
           $table->increments('id');
           $table->string('title');
           $table->time('duration');
           $table->string('soundLink');
           $table->char('gender');
           $table->integer('bpm');
           $table->integer('listening_count');
           $table->integer('price');
           $table->integer('user_id');
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sounds');
    }
}
