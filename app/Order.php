<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model 
{

    protected $table = 'orders';
    public $timestamps = true;

    public function sound()
    {
        return $this->hasOne('Sounds', 'sound_id');
    }

    public function user()
    {
        return $this->hasOne('User', 'user_id');
    }

}