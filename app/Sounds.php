<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sounds extends Model 
{

    protected $table = 'sounds';

    public $timestamps = true;

    protected $fillable = ['soundLink','id','title','duration','gender','bpm','listening_count','price','user_id'];//j'ai ecrit lerreur sur discord pck on a pas de colonne soundLink on estcon
    // protected $fillable = ['*'];


    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

}