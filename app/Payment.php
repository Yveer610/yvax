<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model 
{

    protected $table = 'payments';
    public $timestamps = true;

    public function order()
    {
        return $this->belongsTo('Order', 'order_id');
    }

}