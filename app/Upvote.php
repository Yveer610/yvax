<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upvote extends Model 
{

    protected $table = 'upvotes';
    public $timestamps = true;

    public function sound()
    {
        return $this->belongsTo('Sounds', 'sound_id');
    }

    public function user()
    {
        return $this->hasOne('User', 'user_id');
    }

}