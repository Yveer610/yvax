<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model 
{

    protected $table = 'users';
    public $timestamps = true;

    protected $fillable = ['id','name','firstname','username','email','password','created_at', 'updated_at', 'role'];

}