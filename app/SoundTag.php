<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoundTag extends Model 
{

    protected $table = 'sound_tags';
    public $timestamps = false;

    public function tag()
    {
        return $this->belongsTo('Tag', 'tag_id');
    }

    public function sound()
    {
        return $this->belongsTo('Sounds', 'sound_id');
    }

}