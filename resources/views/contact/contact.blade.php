@extends('layouts.app')

@section('cssContent')
    @include('contact.css')
@endsection

@section('jsContent')
    @include('contact.js')
@endsection