@extends('layouts.app')

@section('cssContent')
    @include('product.css')
@endsection

@section('jsContent')
    @include('product.js')
@endsection