@extends('layouts.app')

@section('cssContent')
    @include('homepage.css')
@endsection

@section('content')
<main class="home">
	<!-- Header -->
	<header id="head">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1 class="lead">YvaX</h1>
                    </div>
                    <hr>
                    <div class="col-12">
                        <p class="tagline">Share You'r sound and get paid !</p>             
                    </div>
                    <div class="col-12">
                    <form method="POST" action="{{route('StoreSound')}}" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Titre') }}</label>

                            <div class="col-md-6">
                                <input id="title" name="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="duration" class="col-md-4 col-form-label text-md-right">{{ __('Durée') }}</label>

                            <div class="col-md-6">
                                <input id="duration" name="duration" type="text" class="form-control{{ $errors->has('duration') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('duration'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('duration') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Genre') }}</label>

                            <div class="col-md-6">
                                <input id="gender" name="gender" type="text" class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                                
                                @if ($errors->has('gender'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="bpm" class="col-md-4 col-form-label text-md-right">{{ __('BPM') }}</label>

                            <div class="col-md-6">
                                <input id="bpm" name="bpm" type="text" class="form-control{{ $errors->has('bpm') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('bpm'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bpm') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Prix') }}</label>

                            <div class="col-md-6">
                                <input id="price" name="price" type="text" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('price'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @csrf
                            <!-- On limite le fichier à 100Ko -->
                            <input type="hidden" name="MAX_FILE_SIZE" value="1000000000">
                            Fichier : <input type="file" name="sound" accept=".mp3">
                            <br>
                            <input type="submit" name="envoyer" value="Envoyer le fichier">
                        </form>
                    </div>
                </div>
            </div>
        </header>
        <!-- /Header -->

        <!-- Intro -->
        <div class="container text-center">
            <br> <br>
            <h2 class="thin">The best place to tell people why they are here</h2>
            <p class="text-muted">
                The difference between involvement and commitment is like an eggs-and-ham breakfast:<br>
                the chicken was involved; the pig was committed.
            </p>
        </div>
        <!-- /Intro-->

        <!-- Highlights - jumbotron -->
        <div class="jumbotron top-space">
            <div class="container">

                <h3 class="text-center thin">Reasons to use this template</h3>

                <div class="row">
                    <div class="col-md-3 col-sm-6 highlight">
                        <div class="h-caption"><h4><i class="fa fa-cogs fa-5"></i>Bootstrap-powered</h4></div>
                        <div class="h-body text-center">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque aliquid adipisci aspernatur. Soluta quisquam dignissimos earum quasi voluptate. Amet, dignissimos, tenetur vitae dolor quam iusto assumenda hic reprehenderit?</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 highlight">
                        <div class="h-caption"><h4><i class="fa fa-flash fa-5"></i>Fat-free</h4></div>
                        <div class="h-body text-center">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores, commodi, sequi quis ad fugit omnis cumque a libero error nesciunt molestiae repellat quos perferendis numquam quibusdam rerum repellendus laboriosam reprehenderit! </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 highlight">
                        <div class="h-caption"><h4><i class="fa fa-heart fa-5"></i>Creative Commons</h4></div>
                        <div class="h-body text-center">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, vitae, perferendis, perspiciatis nobis voluptate quod illum soluta minima ipsam ratione quia numquam eveniet eum reprehenderit dolorem dicta nesciunt corporis?</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 highlight">
                        <div class="h-caption"><h4><i class="fa fa-smile-o fa-5"></i>Author's support</h4></div>
                        <div class="h-body text-center">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, excepturi, maiores, dolorem quasi reprehenderit illo accusamus nulla minima repudiandae quas ducimus reiciendis odio sequi atque temporibus facere corporis eos expedita? </p>
                        </div>
                    </div>
                </div> <!-- /row  -->

            </div>
        </div>
        <!-- /Highlights -->

        <!-- container -->
        <div class="container">

            <h2 class="text-center top-space">Frequently Asked Questions</h2>
            <br>

            <div class="row">
                <div class="col-sm-6">
                    <h3>Which code editor would you recommend?</h3>
                    <p>I'd highly recommend you <a href="http://www.sublimetext.com/">Sublime Text</a> - a free to try text editor which I'm using daily. Awesome tool!</p>
                </div>
                <div class="col-sm-6">
                    <h3>Nice header. Where do I find more images like that one?</h3>
                    <p>
                        Well, there are thousands of stock art galleries, but personally,
                        I prefer to use photos from these sites: <a href="http://unsplash.com">Unsplash.com</a>
                        and <a href="http://www.flickr.com/creativecommons/by-2.0/tags/">Flickr - Creative Commons</a></p>
                </div>
            </div> <!-- /row -->

            <div class="row">
                <div class="col-sm-6">
                    <h3>Can I use it to build a site for my client?</h3>
                    <p>
                        Yes, you can. You may use this template for any purpose, just don't forget about the <a href="http://creativecommons.org/licenses/by/3.0/">license</a>,
                        which says: "You must give appropriate credit", i.e. you must provide the name of the creator and a link to the original template in your work.
                    </p>
                </div>
                <div class="col-sm-6">
                    <h3>Can you customize this template for me?</h3>
                    <p>Yes, I can. Please drop me a line to sergey-at-pozhilov.com and describe your needs in details. Please note, my services are not cheap.</p>
                </div>
            </div> <!-- /row -->

            <div class="jumbotron top-space">
                <h4>Dicta, nostrum nemo soluta sapiente sit dolor quae voluptas quidem doloribus recusandae facere magni ullam suscipit sunt atque rerum eaque iusto facilis esse nam veniam incidunt officia perspiciatis at voluptatibus. Libero, aliquid illum possimus numquam fuga.</h4>
                 <p class="text-right"><a class="btn btn-primary btn-large">Learn more »</a></p>
              </div>

        </div>	<!-- /container -->
    </main>
@endsection

@section('jsContent')
    @include('homepage.js')
@endsection
