@extends('layouts.app')

@section('cssContent')
    @include('profile.css')
@endsection

@section('jsContent')
    @include('profile.js')
@endsection
