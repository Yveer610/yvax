@extends('layouts.app')

@section('cssContent')
    @include('payment.css')
@endsection

@section('jsContent')
    @include('payment.js')
@endsection